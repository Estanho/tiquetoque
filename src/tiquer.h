//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef TIQUER_H_
#define TIQUER_H_

#include <stdio.h>
#include <string.h>

#include <omnetpp.h>
#include <csimplemodule.h>
#include <cmessage.h>

class tiquer: public cSimpleModule {
private:
    cMessage* delayEvent;
    cMessage* networkMessage;

public:
    tiquer();
    virtual ~tiquer();

    int counter;

protected:
    virtual void initialize();
    virtual void handleMessage(cMessage *msg);
};

#endif /* TIQUER_H_ */
