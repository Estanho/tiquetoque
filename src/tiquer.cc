//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include <src/tiquer.h>

Define_Module(tiquer)

tiquer::tiquer() {
    delayEvent = networkMessage = NULL;
}

tiquer::~tiquer() {
    cancelAndDelete(delayEvent);
    if(networkMessage)
        delete networkMessage;
}

void tiquer::initialize() {
    counter = par("limit");

    delayEvent = new cMessage("event");

    WATCH(counter);

    // we no longer depend on the name of the module to decide
    // whether to send an initial message
    if (par("sendMsgOnInit").boolValue() == true)
    {
        EV << getName() << " sending initial message\n";
        networkMessage = new cMessage("tictocMsg");
        scheduleAt(simTime()+2.0, networkMessage);
    }
}
void tiquer::handleMessage(cMessage *msg)
{

    if(msg->isSelfMessage()) {
        if (counter--)    {
            EV << getName() << "'s counter is " << counter << ", sending back message\n";
            send(networkMessage, "out");
            networkMessage = NULL;
        }
        else    {
            EV << getName() << "'s counter reached zero, deleting message\n";
            delete delayEvent;
        }
    } else {
        if(uniform(0,1) < 0.1) {
            EV << "\nMessage lost\n";
            delete msg;
        } else {
            simtime_t delay = par("delayTime");
            networkMessage = msg;
            EV << "\nMEsage received. Waiting " << delay << " before sending the message back\n";
            scheduleAt(simTime()+delay, networkMessage);
        }
    }

}
